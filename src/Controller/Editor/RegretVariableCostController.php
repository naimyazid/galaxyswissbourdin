<?php

namespace App\Controller\Editor;

use App\Entity\VariableCosts;
use App\Security\EmailVerifier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;

/**
 * isGranted("ROLE_EDITOR")
 */
class RegretVariableCostController extends AbstractController
{
    /**
     * @Route("/compte/agent-comptable/refuser-un-frais-hors-forfait/{id}", name="app_regret_variableCost", methods={"POST"})
     * @param variableCosts $variableCost
     * @param Request $request
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @param EmailVerifier $emailVerifier
     * @return JsonResponse| \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function regretVariableCost(VariableCosts $variableCost, Request $request, SessionInterface $session, EntityManagerInterface $entityManager, EmailVerifier $emailVerifier)
    {
        if ($variableCost->getStatus() == 'Validée')
        {
            $session->set('total',$session->get('total') - $variableCost->getPrice());
        }

        $variableCost->setStatus('Refusée');
        $entityManager->flush();

        $emailVerifier->sendEmailConfirmation('app_login', $session->get('visitor'),
            (new TemplatedEmail())
                ->from(new Address('gsb@naim-yazid.fr', 'Galaxy Swiss Bourdin'))
                ->to($session->get('visitor')->getEmail())
                ->subject('Un frais hors forfait Refusé')
                ->htmlTemplate('email/regrettedVariableCost.html.twig')
        );

        if ($request->isXmlHttpRequest())
        {
            return $this->json($variableCost, 200, [], ['groups' => 'variableCost:read']);
        }
        else
        {
            return $this->redirectToRoute('app_process_costs');
        }
    }
}