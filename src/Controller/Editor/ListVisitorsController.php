<?php

namespace App\Controller\Editor;

use App\Entity\SearchUser;
use App\Form\SearchUserType;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * isGranted("ROLE_EDITOR")
 */
class ListVisitorsController extends AbstractController
{
    /**
     * @Route("/compte/liste/visiteurs-medicaux", name="app_list_visitors")
     * @param Security $security
     * @param UserRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function listVisitors(Security $security, UserRepository $repository, PaginatorInterface $paginator, Request $request): Response
    {
        $user = $security->getUser();
        $searchUser = new SearchUser();

        $form = $this->createForm(SearchUserType::class, $searchUser);
        $form->handleRequest($request);
        $visitors = $paginator->paginate(
            $repository->findUsersByRole($searchUser),
            $request->query->getInt('page', 1),
            4
        );

        return $this->render('editor/list_visitors.html.twig', [
            'user' => $user,
            'visitors' => $visitors,
            'form' => $form->createView(),
            'current_menu' => 'list_visitors',
        ]);
    }
}
