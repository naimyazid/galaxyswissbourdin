<?php

namespace App\Controller\Editor;

use App\Entity\Expenses;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


/**
 * isGranted("ROLE_EDITOR")
 */
class AddExpenseController extends AbstractController
{
    /**
     * @Route("/compte/agent-comptable/ajouter-une-note-de-frais", name="app_add_expense")
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function addExpanse(SessionInterface $session, EntityManagerInterface $entityManager): Response
    {
        $user = $this->getUser();
        $visitor = $session->get('visitor');
        $expense = new Expenses();
        $expense->setVisitorFirstName($visitor->getFirstName());
        $expense->setVisitorLastName($visitor->getLastName());
        $expense->setMonth($session->get('month'));
        $expense->setYear($session->get('year'));
        $expense->setTotal($session->get('total'));
        $expense->setIsPayed(false);
        $expense->setUser($user);

        $entityManager->persist($expense);
        $entityManager->flush();

        $session->clear();

        return $this->redirectToRoute('app_process_costs');
    }


}