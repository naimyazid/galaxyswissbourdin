<?php

namespace App\Controller\Editor;


use App\Entity\FixedCosts;
use App\Form\FixedCostType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * isGranted("ROLE_EDITOR")
 */
class EditFixedCostController extends AbstractController
{
    /**
     * @Route("/compte/agent-comptable/modifier-un-frais-au-forfait/{id}", name="app_edite_fixedCost", methods={"POST|GET"})
     * @param FixedCosts $fixedCost
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param SessionInterface $session
     * @return Response
     */
    public function validateFixedCost(FixedCosts $fixedCost, Request $request, EntityManagerInterface $entityManager, SessionInterface $session): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(FixedCostType::class, $fixedCost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $entityManager->flush();
            if ($request->isXmlHttpRequest()) {
                return $this->json($fixedCost, 200, [], ['groups' => 'fixedCost:read']);
            } else {
                return $this->redirectToRoute('app_process_costs');
            }
        }

        return $this->render('includes/_form_edit_cost.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'current_menu' => 'process_costs'
        ]);
    }
}