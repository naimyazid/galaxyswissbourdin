<?php

namespace App\Controller\Editor;

use App\Entity\VariableCosts;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * isGranted("ROLE_EDITOR")
 */
class ValidateVariableCostController extends AbstractController
{
    /**
     * @Route("/compte/agent-comptable/valider-un-frais-hors-forfait/{id}", name="app_validate_variableCost", methods="POST")
     * @param variableCosts $variableCost
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param SessionInterface $session
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function validateVariableCost(VariableCosts $variableCost, Request $request, EntityManagerInterface $entityManager, SessionInterface $session)
    {
        if($variableCost->getStatus() !== 'Validée')
        {
            $session->set('total',$session->get('total') + $variableCost->getPrice());
        }

        $variableCost->setStatus('Validée');
        $entityManager->flush();

        if ($request->isXmlHttpRequest())
        {
            return $this->json($variableCost, 200, [], ['groups' => 'variableCost:read']);
        }
        else
        {
            return $this->redirectToRoute('app_process_costs');
        }
    }
}