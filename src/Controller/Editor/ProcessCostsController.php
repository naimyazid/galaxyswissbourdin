<?php

namespace App\Controller\Editor;

use App\Entity\SearchCosts;
use App\Form\SearchCostByUserType;
use App\Repository\FixedCostsRepository;
use App\Repository\VariableCostsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


/**
 * isGranted("ROLE_EDITOR")
 */
class ProcessCostsController extends AbstractController
{
    /**
     * @Route("/compte/agent-comptable/traiter-une-fiches-de-frais", name="app_process_costs")
     * @param SessionInterface $session
     * @param FixedCostsRepository $fixedCostsRepository
     * @param VariableCostsRepository $variableCostsRepository
     * @param Request $request
     * @return Response
     */
    public function processCosts(SessionInterface $session,
                                 FixedCostsRepository $fixedCostsRepository,
                                 VariableCostsRepository $variableCostsRepository,
                                 Request $request
    ): Response

    {
        $user = $this->getUser();
        $fixedCosts = null;
        $variableCosts = null;
        $visitor = null;
        $message = null;
        $searchCosts = new SearchCosts();
        $form = $this->createForm(SearchCostByUserType::class, $searchCosts);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $visitor = $form->get('user')->getData();
            $month = $form->get('month')->getData();
            $year = $form->get('year')->getData();
            $session->set('visitor', $visitor);
            $session->set('month', $month);
            $session->set('year', $year);
            $fixedCosts = $fixedCostsRepository->findByDate($searchCosts);
            $variableCosts = $variableCostsRepository->findByDate($searchCosts);
            if ($fixedCosts == null && $variableCosts == null)
            {
                $message  = 'Le visiteur médical : '.$visitor->getFirstName().' '.$visitor->getLastName().' n\'a déclaré aucun frais pour le mois de '. $month.'-'.$year.' !';
            }

        }
        return $this->render('editor/process_costs.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'visitor' => $visitor,
            'fixedCosts' => $fixedCosts,
            'variableCosts' => $variableCosts,
            'message' => $message,
            'current_menu' => 'process_costs'
        ]);
    }
}
