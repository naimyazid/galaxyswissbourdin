<?php

namespace App\Controller\Editor;

use App\Repository\ExpensesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * isGranted("ROLE_EDITOR")
 */
class ShowExpensesController extends AbstractController
{
    /**
     * @Route("/compte/agent-comptable/notes-de-frais", name="app_show_expenses")
     * @param ExpensesRepository $repository
     * @return Response
     */
    public function addExpanse(ExpensesRepository $repository): Response
    {
        $user = $this->getUser();
        $expenses = $repository->findExpansesNotPayed();

        return $this->render('editor/expenses.html.twig', [
            'user' => $user,
            'current_menu' => 'list_expenses',
            'expenses' => $expenses,
        ]);
    }


}