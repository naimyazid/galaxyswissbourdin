<?php

namespace App\Controller\Editor;

use App\Entity\Expenses;
use App\Entity\SearchCosts;
use App\Repository\FixedCostsRepository;
use App\Repository\VariableCostsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


/**
 * isGranted("ROLE_EDITOR")
 */
class ExpenseDetailController extends AbstractController
{
    /**
     * @Route("/compte/agent-comptable/détail-note-de-frais/{id}", name="app_detail_expense")
     * @param Expenses $expense
     * @param SessionInterface $session
     * @param FixedCostsRepository $fixedCostsRepository
     * @param VariableCostsRepository $variableCostsRepository
     * @return Response
     */
    public function detailExpanse(Expenses $expense, SessionInterface $session, FixedCostsRepository $fixedCostsRepository, VariableCostsRepository $variableCostsRepository ): Response
    {
        $user = $this->getUser();
        $month = $expense->getMonth();
        $year = $expense->getYear();
        $visitor = $session->get('visitorExpense');
        $searchCosts = new SearchCosts();
        $searchCosts->setUser($visitor)
                    ->setMonth($month)
                    ->setYear($year);
        $fixedCosts = $fixedCostsRepository->findByDate($searchCosts);
        $variableCosts = $variableCostsRepository->findByDate($searchCosts);

        return $this->render('editor/visitor_expense_detail.html.twig', [
            'user' => $user,
            'visitor' => $visitor,
            'month' => $month,
            'year' => $year,
            'expense' => $expense,
            'fixedCosts' => $fixedCosts,
            'variableCosts' => $variableCosts,
            'current_menu' => 'list_visitors',
        ]);
    }


}