<?php

namespace App\Controller\Editor;

use App\Entity\FixedCosts;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * isGranted("ROLE_EDITOR")
 */
class ValidateFixedCostController extends AbstractController
{


    /**
     * @Route("/compte/agent-comptable/valider-un-frais-au-forfait/{id}", name="app_validate_fixedCost", methods="POST")
     * @param FixedCosts $fixedCost
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param SessionInterface $session
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function validateFixedCost(FixedCosts $fixedCost, Request $request, EntityManagerInterface $entityManager, SessionInterface $session)
    {
        if($fixedCost->getStatus() !== 'Validée')
        {
            $session->set('total', $session->get('total') + ($fixedCost->getQuantity() * $fixedCost->getDescriptionCost()->getFixedPrice()));
        }
        $fixedCost->setStatus('Validée');
        $entityManager->flush();

        if ($request->isXmlHttpRequest()) {
            return $this->json($fixedCost, 200, [], ['groups' => 'fixedCost:read']);
        } else {
            return $this->redirectToRoute('app_process_costs');
        }

    }
}