<?php

namespace App\Controller\Editor;

use App\Repository\ExpensesRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


/**
 * isGranted("ROLE_EDITOR")
 */
class ExpensesVisitorController extends AbstractController
{
    /**
     * @Route("/compte/agent-comptable/notes-de-frais/{id}", name="app_visitor_show_expenses")
     * @param $id
     * @param ExpensesRepository $expensesRepository
     * @param UserRepository $userRepository
     * @param SessionInterface $session
     * @return Response
     */
    public function addExpanse($id, ExpensesRepository $expensesRepository, UserRepository $userRepository, SessionInterface $session): Response
    {
        $user = $this->getUser();
        $visitor = $userRepository->findOneById(['id' => $id]);
        if (null == $visitor){
            return $this->redirectToRoute('app_list_visitors');
        }
        $expenses = $expensesRepository->findExpansesByUser($visitor);

        $session->set('visitorExpense', $visitor);

        return $this->render('editor/visitor_expenses.html.twig', [
            'user' => $user,
            'visitor' => $visitor,
            'current_menu' => 'list_visitors',
            'expenses' => $expenses,
        ]);
    }


}