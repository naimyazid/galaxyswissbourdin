<?php

namespace App\Controller\Editor;

use App\Entity\FixedCosts;
use App\Security\EmailVerifier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;

/**
 * isGranted("ROLE_EDITOR")
 */
class RegretFixedCostController extends AbstractController
{
    /**
     * @Route("/compte/agent-comptable/refuser-un-frais-au-forfait/{id}", name="app_regret_fixedCost", methods={"POST"})
     * @param FixedCosts $fixedCost
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param SessionInterface $session
     * @param EmailVerifier $emailVerifier
     * @return JsonResponse | \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function regretFixedCost(FixedCosts $fixedCost, Request $request, EntityManagerInterface $entityManager, SessionInterface $session, EmailVerifier $emailVerifier)
    {
        if ($fixedCost->getStatus() == 'Validée')
        {
            $session->set('total', $session->get('total') - ($fixedCost->getQuantity() * $fixedCost->getDescriptionCost()->getFixedPrice()));
        }
        $fixedCost->setStatus('Refusée');
        $entityManager->flush();

        $emailVerifier->sendEmailConfirmation('app_login', $session->get('visitor'),
            (new TemplatedEmail())
                ->from(new Address('gsb@naim-yazid.fr', 'Galaxy Swiss Bourdin'))
                ->to($session->get('visitor')->getEmail())
                ->subject('Un frais au forfait Refusé')
                ->htmlTemplate('email/regrettedFixedCost.html.twig')
        );

        if ($request->isXmlHttpRequest())
        {
            return $this->json($fixedCost, 200, [], ['groups' => 'fixedCost:read']);
        }
        else
        {
            return $this->redirectToRoute('app_process_costs');
        }
    }
}