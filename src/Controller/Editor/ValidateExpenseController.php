<?php

namespace App\Controller\Editor;

use App\Entity\Expenses;
use App\Entity\FixedCosts;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * isGranted("ROLE_EDITOR")
 */
class ValidateExpenseController extends AbstractController
{


    /**
     * @Route("/compte/agent-comptable/valider-une-de-frais/{id}", name="app_validate_expense", methods="POST|GET")
     * @param Expenses $expense
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function validateExpense(Expenses $expense, Request $request, EntityManagerInterface $entityManager)
    {
        $expense->setIsPayed(true);
        $entityManager->flush();

        if ($request->isXmlHttpRequest()) {
            return $this->json($expense, 200, [], ['groups' => 'expense:read']);
        } else {
            return $this->redirectToRoute('app_show_expenses');
        }

    }
}