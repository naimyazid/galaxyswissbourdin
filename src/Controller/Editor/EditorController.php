<?php

namespace App\Controller\Editor;

use App\Entity\Avatar;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * isGranted("ROLE_EDITOR")
 */
class EditorController extends AbstractController
{
    /**
     * @Route("/compte/agent-comptable", name="app_account_editor")
     * @param Security $security
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function index(Security $security, EntityManagerInterface $entityManager): Response
    {
        $user = $security->getUser();
        $photo = $user->getPhoto();
        $avatar = $entityManager->getRepository(Avatar::class)->findOneBy(['avatar' => $photo]);
        return $this->render('editor/index.html.twig', [
            'user' => $user,
            'avatar' => $avatar,
            'current_menu' => 'profile'
        ]);
    }
}
