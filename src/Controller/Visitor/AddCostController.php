<?php

namespace App\Controller\Visitor;


use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * isGranted("ROLE_USER")
 */
class AddCostController extends AbstractController
{
    /**
     * @Route("/compte/visiteur-medical/saisir-mes-notes-de-frais", name="app_add_costs")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function addCost( Request $request, EntityManagerInterface $entityManager): Response
    {
        $user = $this->getUser();
        $date = new \DateTime();
        $currentDay = $date->format('d');

        // Clôture de la fiche de frais du mois qui vient de s'écoulé
        if ($currentDay == 1)
        {
            foreach ($user->getFixedCosts() as $fixedCost)
            {
                $fixedCost->setIsClosed(true);
                $entityManager->flush();
            }
            foreach ($user->getVariableCosts() as $variableCost)
            {
                $variableCost->setIsClosed(true);
                $entityManager->flush();
            }

        }

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $entityManager->flush();
        }

        return $this->render('visitor/add_cost.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'current_menu' => 'create_cost',
        ]);
    }
}

