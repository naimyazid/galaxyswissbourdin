<?php

namespace App\Controller\Visitor;

use App\Entity\SearchCosts;
use App\Form\SearchCostType;
use App\Repository\FixedCostsRepository;
use App\Repository\VariableCostsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * isGranted("ROLE_USER")
 */
class ShowCostController extends AbstractController
{
    /**
     * @Route("/compte/visiteur-medical/consulter-mes-notes-de-frais", name="app_show_costs")
     * @param Request $request
     * @param FixedCostsRepository $fixedCostsRepository
     * @param VariableCostsRepository $variableCostsRepository
     * @return Response
     */
    public function showCosts(Request $request, FixedCostsRepository $fixedCostsRepository, VariableCostsRepository $variableCostsRepository): Response
    {
        $message = null;
        $month = null;
        $year = null;
        $fixedCosts = null;
        $variableCosts = null;

        $user = $this->getUser();

        $searchCosts = new searchCosts();

        $form = $this->createForm(SearchCostType::class, $searchCosts );
        $form->HandleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $searchCosts->setUser($user);
            $month = $form->get('month')->getData();
            $year = $form->get('year')->getData();
            $fixedCosts = $fixedCostsRepository->findByDate($searchCosts);
            $variableCosts = $variableCostsRepository->findByDate($searchCosts);
            if ($fixedCosts == null && $variableCosts == null)
            {
                $message  = 'Vous n\'avez déclaré aucun frais pour le mois de '. $month.'-'.$year.' !';
            }
        }

        return $this->render('visitor/consulter_notes_de_frais.html.twig', [
            'user' => $user,
            'fixedCosts' => $fixedCosts,
            'variableCosts' => $variableCosts,
            'message' => $message,
            'form' => $form->createView(),
            'current_menu' => 'show_cost',
            'month' => $month,
            'year' => $year
        ]);
    }
}
