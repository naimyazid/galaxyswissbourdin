<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;


class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField:: new ('firstName', 'Nom'),
            TextField:: new ('lastName', 'Prénom'),
            EmailField:: new ('email', 'Email'),
            ChoiceField:: new ('roles', 'Rôle')
                ->allowMultipleChoices()
                ->setChoices(['ROLE_USER' => 'ROLE_USER',
                              'ROLE_EDITOR' => 'ROLE_EDITOR',
                              'ROLE_ADMIN' => 'ROLE_ADMIN']),
            DateTimeField:: new ('createdAt', 'Date d\'inscription'),
            BooleanField:: new ('isVerified', 'Compte validé'),
            TextField:: new ('sex', 'Sexe' ),
            DateField:: new ('dateOfBirth', 'Date de naissance'),
            TextField:: new ('phoneNumber', 'Téléphone'),
        ];
    }

}

