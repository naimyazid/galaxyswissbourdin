<?php

namespace App\Controller\Admin;

use App\Entity\Expenses;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ExpensesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Expenses::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
