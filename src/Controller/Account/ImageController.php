<?php

namespace App\Controller\Account;

use App\Entity\Avatar;
use App\Form\ImageType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ImageController extends AbstractController
{
    private $security;
    private $request;
    private $entityManager;

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }


    /**
     * @Route("/compte/ajouter/photo-de-profil", name="app_add_photo")
     * @param Request $request
     * @return Response
     */
    public function addPhoto(Request $request): Response
    {
        $user = $this->security->getUser();
        $avatar = new Avatar();
        $form = $this->createForm(ImageType::class, $avatar);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $this->entityManager->persist($avatar);
            $this->entityManager->flush();

            $user->setPhoto($avatar->getAvatar());
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            if ($user->hasRole('ROLE_EDITOR'))
            {
                return $this->redirectToRoute('app_account_editor');
            }

            if ($user->hasRole('ROLE_USER'))
            {
                return $this->redirectToRoute('app_account_visitor');
            }
        }
        if($user->hasRole('ROLE_EDITOR'))
        {
            return $this->render('editor/add_image.html.twig', [
                'current_menu' => 'profile',
                'user' => $user,
                'form' => $form->createView()
            ]);
        }
        if ($user->hasRole('ROLE_USER'))
        {
            return $this->render('visitor/add_image.html.twig', [
                'current_menu' => 'profile',
                'user' => $user,
                'form' => $form->createView()
            ]);
        }
    }


    /**
     * @Route("/compte/modifier/photo-de-profil/{id}", name="app_edit_photo", methods="GET|POST")
     * @param Avatar $avatar
     * @param Request $request
     * @return Response
     */
    public function editPhoto(Avatar $avatar, Request $request): Response
    {
        $user = $this->security->getUser();
        $form = $this->createForm(ImageType::class, $avatar);
        $form->HandleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $this->entityManager->flush();
            $user->setPhoto($avatar->getAvatar());
            $this->entityManager->flush();

            if ($user->hasRole('ROLE_EDITOR'))
            {
                return $this->redirectToRoute('app_account_editor');
            }

            if ($user->hasRole('ROLE_USER'))
            {
                return $this->redirectToRoute('app_account_visitor');
            }
        }
        if ($user->hasRole('ROLE_EDITOR'))
        {
            return $this->render('editor/add_image.html.twig', [
                'current_menu' => 'profile',
                'avatar' => $avatar,
                'user' => $user,
                'form' => $form->createView()
            ]);
        }
        if ($user->hasRole('ROLE_USER'))
        {
            return $this->render('visitor/add_image.html.twig', [
                'current_menu' => 'profile',
                'avatar' => $avatar,
                'user' => $user,
                'form' => $form->createView()
            ]);
        }
    }

    /**
     * @Route("/compte/supprimer/photo-de-profil/{id}", name="app_delete_photo", methods="DELETE")
     * @param Avatar $avatar
     * @param Request $request
     */
    public function deletePhoto(Avatar $avatar, Request $request)
    {
        $user = $this->security->getUser();
        if ($this->isCsrfTokenValid('delete'. $avatar->getId(), $request->get('_token'))) {
            $this->entityManager->remove($avatar);
            $this->entityManager->flush();

            $user->setPhoto(null);
            $this->entityManager->flush();
        }
        if ($user->hasRole('ROLE_EDITOR'))
        {
            return $this->redirectToRoute('app_account_editor');
        }

        if ($user->hasRole('ROLE_USER'))
        {
            return $this->redirectToRoute('app_account_visitor');
        }
    }
}
