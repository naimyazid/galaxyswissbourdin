<?php

namespace App\Controller\Account;

use App\Form\EditeProfileType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class EditeProfileController extends AbstractController
{
    /**
     * @Route("/edite/profile", name="app_edite_profile")
     * @param Security $security
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function editProfile(Security $security, Request $request, EntityManagerInterface $entityManager): Response
    {
        $user = $security->getUser();
        $form = $this->createForm(EditeProfileType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $entityManager->flush();

            if ($user->hasRole('ROLE_EDITOR'))
            {
                return $this->redirectToRoute('app_account_editor');
            }

            if ($user->hasRole('ROLE_USER'))
            {
                return $this->redirectToRoute('app_account_visitor');
            }
        }

        if ($user->hasRole('ROLE_EDITOR'))
        {
            return $this->render('editor/profile.html.twig', [
                'user' => $user,
                'form' => $form->createView(),
                'current_menu' => 'profile'
            ]);
        }
        if ($user->hasRole('ROLE_USER'))
        {
            return $this->render('visitor/profile.html.twig', [
                'user' => $user,
                'form' => $form->createView(),
                'current_menu' => 'profile'
            ]);
        }
    }
}
