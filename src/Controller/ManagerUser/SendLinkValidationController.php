<?php

namespace App\Controller\ManagerUser;

use App\Entity\User;
use App\Form\SecurityType;
use App\Security\EmailVerifier;
use Symfony\Component\Mime\Address;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SendLinkValidationController extends AbstractController
{
    private $emailVerifier;
    private $entityManager;

    public function __construct(EmailVerifier $emailVerifier, EntityManagerInterface $entityManager)
    {
        $this->emailVerifier = $emailVerifier;
        $this->entityManager = $entityManager;
    }


    /**
     * @Route("/nouveau/lien/validation", name="send_link_validation")
     */
    public function index(Request $request): Response
    {
        $form = $this->createForm(SecurityType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {

            $user = $this->entityManager->getRepository(User::class)->findOneByEmail($form->get('email')->getData());

            if ($user === null)
            {
                $this->addFlash('danger', 'Aucun compte n\'est associé à cette adresse email !');
                return $this->redirectToRoute('send_link_validation');
            }

            if ($user->isVerified() === true)
            {
                $this->addFlash('danger', 'Votre compte est déja activé !');
                return $this->redirectToRoute('send_link_validation');
            }

            else
            {
                // generate a signed url and email it to the user
                $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user,
                    (new TemplatedEmail())
                        ->from(new Address('gsb@naim-yazid.fr', 'Galaxy Swiss Bourdin'))
                        ->to($user->getEmail())
                        ->subject('Veuillez confirmer votre email')
                        ->htmlTemplate('email/confirmation_email.html.twig')
                );
                // do anything else you need here, like send an email
                $this->addFlash('success', 'Vous allez recevoir un mail pour activer votre compte !');

                return $this->redirectToRoute('send_link_validation');
            }
        }
        return $this->render('security/sendLink.html.twig', [
            'sendLink' => $form->createView()
        ]);
    }
}

