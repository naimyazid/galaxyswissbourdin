<?php

namespace App\Controller\ManagerUser;

use App\Entity\User;
use App\Form\SecurityType;
use App\Form\PasswordResetType;
use App\Security\EmailVerifier;
use Symfony\Component\Mime\Address;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class ForgetPasswordController extends AbstractController
{
    private $emailVerifier;
    private $entityManager;
    private $translator;

    public function __construct(EmailVerifier $emailVerifier, EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->emailVerifier = $emailVerifier;
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }


    /**
     * Si l'utilisateur éxiste il reçoit un mail avec un lien de redirection    vers le formaulaire de réinitailisation du mot de passe.
     *
     * @Route("/mot-de-passe/oublié", name="app_forget_password")
     * @param Request $request
     * @return Response
     */
    public function forgetPassword(Request $request): Response
    {
        $form = $this->createForm(SecurityType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $user = $this->entityManager->getRepository(User::class)->findOneByEmail($form->get('email')->getData());

            if ($user === null)
            {
                $this->addFlash('danger', 'Aucun compte n\'est associé à cette adresse email !');
                return $this->redirectToRoute('app_forget_password');
            }
            else
            {
                // generate a signed url and email it to the user
                $this->emailVerifier->sendEmailConfirmation('app_reset_password', $user,
                    (new TemplatedEmail())
                        ->from(new Address('gsb@naim-yazid.fr', 'Galaxy Swiss Bourdin'))
                        ->to($user->getEmail())
                        ->subject('Réinitialisation du mot de passe')
                        ->htmlTemplate('email/forget_password.html.twig')
                );
                // do anything else you need here, like send an email
                $this->addFlash('success', 'Vous allez recevoir un mail pour réinitialiser votre mot de passe !');
                return $this->redirectToRoute('app_forget_password');
            }
        }
        return $this->render('security/forget_password.html.twig', [
            'sendLink' => $form->createView()
        ]);
    }


    /**
     * Si le lien de réinitialisation du mot de passes est valide, l'utilisateur est redirigé vers le formulaire prévu à cet effet, sinon, on informe ce dernier que le lien est expiré et on l'invite à renouveler sa demande
     *
     * @Route("/Nouvea/mot-de-passe", name="app_reset_password")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function resetPassword(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $id = $request->get('id');
        $user = $this->entityManager->getRepository(User::class)->find($id);
        // validate email confirmation link,
        try
        {
            $this->emailVerifier->handleEmailConfirmation($request, $user);
            // $user = $this->entityManager->getRepository(User::class)->find($id);

            $form = $this->createForm(PasswordResetType::class, $user);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid())
            {
                $userForm = $form->getData();

                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $userForm,
                        $userForm->getPassword()
                    )
                );

                $user->setLastLoginAt(new \DateTime());

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                $this->addFlash('success', 'Votre mot de passe est mis à jour !');

                return $this->redirectToRoute('app_login');
            }
            return $this->render('security/newPassword.html.twig', [
                'form' => $form->createView()
            ]);
        }
        catch (VerifyEmailExceptionInterface $Exception)
        {
            $message = $this->translator->trans('verify_password_error');
            $this->addFlash('message', $message);

            return $this->redirectToRoute('app_forget_password');
        }
    }
}

