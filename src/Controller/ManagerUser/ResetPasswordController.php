<?php

namespace App\Controller\ManagerUser;

use App\Form\PasswordNewType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class ResetPasswordController extends AbstractController
{
    /**
     * @Route("/compte/nouveau-mot-de-passe", name="app_new_password")
     * @param Security $security
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function newPassword(Security $security, Request $request, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = $security->getUser();
        $form = $this->createForm(PasswordNewType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $oldPassword = $form->get('old_password')->getData();
            if ($passwordEncoder->isPasswordValid($user, $oldPassword))
            {
                $newPassword = $form->get('new_password')->getData();
                $password = $passwordEncoder->encodePassword($user, $newPassword);
                $user->setPassword($password);
                $entityManager->flush();

                $this->addFlash('success', 'Votre mot de passe a bien été mis à jour.');
            }
            else
            {
                $this->addFlash('danger', 'Votre ancien mot de passe n\'est pas valide.');
            }
        }
        if ($user->hasRole('ROLE_EDITOR')){
            return $this->render('editor/new_password.html.twig', [
                'user' => $user,
                'form' => $form->createView(),
                'current_menu' => 'new_password'
            ]);
        }
        if($user->hasRole('ROLE_USER'))
        {
            return $this->render('visitor/new_password.html.twig', [
                'user' => $user,
                'form' => $form->createView(),
                'current_menu' => 'new_password'
            ]);
        }
    }
}
