<?php

namespace App\Form;

use App\Entity\SearchUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => false,
                'required' => false,
                'attr'=> [
                    'placeholder' => 'Nom',
                    'class'=> 'border-0'
                ]
            ])
            ->add('lastName', TextType::class, [
                'label' => false,
                'required' => false,
                'attr'=> [
                    'placeholder' => 'Prénom',
                    'class'=> 'border-0'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SearchUser::class,
            'method' => 'Get',
        ]);
    }
}