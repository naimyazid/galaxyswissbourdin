<?php

namespace App\Form;

use App\Entity\SearchCosts;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchCostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('month', ChoiceType::class, [
                'choices' => [
                    'Janvier'=>'01',
                    'Février'=>'02',
                    'Mars'=>'03',
                    'Avril'=>'04',
                    'Mai' => '05',
                    'Juin' => '06',
                    'Juillet' => '07',
                    'Août' => '08',
                    'Septembre' => '09',
                    'Octobre' => '10',
                    'Noviembre' => '11',
                    'Décembre' => '12'
                ],
                'label' =>false,
                'attr' => [
                    'class' => 'form-control-sm inline-input mt-3',
                    'placeholder' => 'Mois',
                    'min' => 1,
                    'max' => 12
                ]
            ])
            ->add('year', ChoiceType::class, [
                'choices' => [
                    '2015'=>'2015',
                    '2016'=>'2016',
                    '2017'=>'2017',
                    '2018'=>'2018',
                    '2019' => '2019',
                    '2020' => '2020',
                    '2021' => '2021'
                ],
                'label' => false,
                'attr' => [
                    'class' => 'form-control-sm inline-input mt-3',
                    'placeholder' => 'Année',
                    'min' => 2000,
                    'max' => 2050
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SearchCosts::class,
            'method' => 'POST',
        ]);
    }
}
