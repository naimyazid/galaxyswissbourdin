<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;


class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => false,
                'attr'=> [
                    'placeholder' => 'Nom*',
                    'class'=> 'border-0'
                ]
            ])
            ->add('lastName', TextType::class, [
                'label' => false,
                'attr'=> [
                    'placeholder' => 'Prénom*',
                    'class'=> 'border-0'
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => false,
                'attr'=> [
                    'placeholder' => 'Email*',
                    'class'=> 'border-0'
                ]
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'constraints' => new Regex([
                    'pattern' =>"/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!*$@%_])([-+!*$@%_\w]{8,15})$/",
                    'message' =>'Votre mot de passe doit contenir au moins 8 caractères, dont un caractère en majuscule et au moins un de ces caractères spéciaux {-+!*$@%_}'
                ]),
                'invalid_message' => 'Votre mot de passe et la confirmation doivent être identiques ',
                'first_options' => [
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'Mot de passe',
                        'class'=> 'border-0'
                    ]
                ],
                'second_options' => [
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'Confirmez votre mot de passe',
                        'class'=> 'border-0'
                    ]
                ],

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
