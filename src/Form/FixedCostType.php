<?php

namespace App\Form;

use App\Entity\Cost;
use App\Entity\FixedCosts;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FixedCostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantity', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'form-control-sm',
                    'placeholder' => 'Sisissez la quantitié'
                ]
            ])
            ->add('unitPrice', textType::class, [
                'label' => false,
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'class' => 'form-control-sm bg-light',
                    'readonly' => true
                ]
            ])
            ->add('total', TextType::class, [
                'label' => false,
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'class' => 'form-control-sm bg-light totalFixedCost',
                    'readonly' => true
                ]
            ])
            ->add('descriptionCost', EntityType::class, [
                'label' => false,
                'class' => Cost::class,
                'choice_label' => 'name',
                'choice_value' => 'fixedPrice',
                'placeholder' =>'Choisir un frais',
                'attr' => [
                    'class' => 'form-control-sm'
                ]
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FixedCosts::class,
        ]);
    }
}
