<?php

namespace App\DataFixtures;

use App\Entity\Cost;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CostFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
       for($i=0; $i<10; $i++)
       {
           $cost = new Cost();
           $cost->setName(sprintf('Frais au forfait numéro %d', $i))
                ->setFixedPrice(rand(0,100));

           $manager->persist($cost);
       }
       $manager->flush();
    }
}
