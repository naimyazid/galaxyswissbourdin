<?php

namespace App\Entity;

use App\Repository\ExpensesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ExpensesRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Expenses
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("expense:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("expense:read")
     */
    private $visitorFirstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("expense:read")
     */
    private $visitorLastName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("expense:read")
     */
    private $month;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("expense:read")
     */
    private $year;

    /**
     * @ORM\Column(type="integer", length=255)
     * @Groups("expense:read")
     */
    private $total;

    /**
     * @ORM\Column(type="boolean")
     * @Groups("expense:read")
     */
    private $isPayed;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("expense:read")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("expense:read")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="expenses")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreated()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdated()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    public function getVisitorFirstName(): ?string
    {
        return $this->visitorFirstName;
    }

    public function setVisitorFirstName(string $visitorFirstName): self
    {
        $this->visitorFirstName = $visitorFirstName;

        return $this;
    }

    public function getVisitorLastName(): ?string
    {
        return $this->visitorLastName;
    }

    public function setVisitorLastName(string $visitorLastName): self
    {
        $this->visitorLastName = $visitorLastName;

        return $this;
    }

    public function getMonth(): ?string
    {
        return $this->month;
    }

    public function setMonth(string $month): self
    {
        $this->month = $month;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(string $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getTotal(): ?string
    {
        return $this->total;
    }

    public function setTotal(string $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getIsPayed(): ?bool
    {
        return $this->isPayed;
    }

    public function setIsPayed(bool $isPayed): self
    {
        $this->isPayed = $isPayed;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
