<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;

class SearchCosts
{
    /**
     * @Assert\LessThan(13)
     * @Assert\GreaterThan(0)
     */
    private $month;

    /**
     * @Assert\LessThan(2050)
     * @Assert\GreaterThan(2000)
     */
    private $year;

    /**
     * @var
     */
    private $user;

    /**
     * @return mixed
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @param mixed $month
     */
    public function setMonth(int $month): SearchCosts
    {
        $this->month = $month;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear(int $year): SearchCosts
    {
        $this->year = $year;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): SearchCosts
    {
        $this->user = $user;
        return $this;
    }

}