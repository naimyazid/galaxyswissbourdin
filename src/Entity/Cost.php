<?php

namespace App\Entity;

use App\Repository\CostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CostRepository::class)
 */
class Cost
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $fixedPrice;

    /**
     * @ORM\OneToMany(targetEntity=FixedCosts::class, mappedBy="descriptionCost")
     */
    private $fixedCosts;

    public function __construct()
    {
        $this->fixedCosts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFixedPrice(): ?float
    {
        return $this->fixedPrice;
    }

    public function setFixedPrice(float $fixedPrice): self
    {
        $this->fixedPrice = $fixedPrice;

        return $this;
    }

    /**
     * @return Collection|FixedCosts[]
     */
    public function getFixedCosts(): Collection
    {
        return $this->fixedCosts;
    }

    public function addFixedCost(FixedCosts $fixedCost): self
    {
        if (!$this->fixedCosts->contains($fixedCost)) {
            $this->fixedCosts[] = $fixedCost;
            $fixedCost->setDescriptionCost($this);
        }

        return $this;
    }

    public function removeFixedCost(FixedCosts $fixedCost): self
    {
        if ($this->fixedCosts->removeElement($fixedCost)) {
            // set the owning side to null (unless already changed)
            if ($fixedCost->getDescriptionCost() === $this) {
                $fixedCost->setDescriptionCost(null);
            }
        }

        return $this;
    }
}
