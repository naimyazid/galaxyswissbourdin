<?php

namespace App\Entity;

use App\Repository\FixedCostsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=FixedCostsRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class FixedCosts
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("fixedCost:read")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups("fixedCost:read")
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity=Cost::class, inversedBy="fixedCosts")
     */
    private $descriptionCost;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("fixedCost:read")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("fixedCost:read")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("fixedCost:read")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="fixedCosts")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isClosed;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreated()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
        $this->setIsClosed(false);
        if(empty($this->status))
        {
            $this->setStatus('Envoyée');
        }
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdated()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getDescriptionCost(): ?Cost
    {
        return $this->descriptionCost;
    }

    public function setDescriptionCost(?Cost $descriptionCost): self
    {
        $this->descriptionCost = $descriptionCost;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }


    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIsClosed(): ?bool
    {
        return $this->isClosed;
    }

    public function setIsClosed(?bool $isClosed): self
    {
        $this->isClosed = $isClosed;

        return $this;
    }
}
