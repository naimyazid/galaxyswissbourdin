<?php

namespace App\Repository;

use App\Entity\SearchUser;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     * @param UserInterface $user
     * @param string $newEncodedPassword
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * @return Criteria
     */
    public static function getFiltredCosts(): Criteria
    {
        return Criteria::create()
            //-> andWhere(Criteria::expr()->gt('createdAt', new \DateTime('-10days')));
            -> andWhere(Criteria::expr()->eq('isClosed', false));
    }

    /**
     * @param SearchUser $searchUser
     * @return Query
     */
    public function findUsersByRole(SearchUser $searchUser): Query
    {
        $req = $this->createQueryBuilder('u');
            if ($searchUser->getFirstName())
            {
                $req = $req->andWhere('u.firstName = :firstName')
                    ->setParameter('firstName', $searchUser->getFirstName())
                    ->andWhere('u.roles LIKE :role')
                    ->setParameter('role', '%'.'ROLE_USER'.'%');
            }
            if ($searchUser->getLastName())
            {
                $req = $req->andWhere('u.lastName = :lastName')
                    ->setParameter('lastName', $searchUser->getLastName())
                    ->andWhere('u.roles LIKE :role')
                    ->setParameter('role', '%'.'ROLE_USER'.'%');
            }
            else
            {
                $req = $req->andWhere('u.roles LIKE :role')
                    ->setParameter('role', '%'.'ROLE_USER'.'%');
            }
        return $req->getQuery();
    }



    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
