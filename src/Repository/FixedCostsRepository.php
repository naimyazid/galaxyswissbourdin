<?php

namespace App\Repository;

use App\Entity\FixedCosts;
use App\Entity\SearchCosts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FixedCosts|null find($id, $lockMode = null, $lockVersion = null)
 * @method FixedCosts|null findOneBy(array $criteria, array $orderBy = null)
 * @method FixedCosts[]    findAll()
 * @method FixedCosts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FixedCostsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FixedCosts::class);
    }

    /**
     * @param SearchCosts $searchCosts
     * @return int|mixed|string
     * @throws \Exception
     */

    public function findByDate(SearchCosts $searchCosts)
    {
        $req = $this->createQueryBuilder('f');
            if($searchCosts->getMonth() && $searchCosts->getYear()){
                $req = $req->andWhere('f.user = :user')
                           ->setParameter('user', $searchCosts->getUser())
                           ->andWhere('f.createdAt >= :createdBegin')
                           ->setParameter('createdBegin', new \DateTime($searchCosts->getYear().'-'. $searchCosts->getMonth().'-01' ))
                           ->andWhere('f.createdAt <= :createdEnd')
                           ->setParameter('createdEnd', new \DateTime($searchCosts->getYear().'-'. $searchCosts->getMonth().'-31' ));
            }
        return $req->getQuery()->getResult();

    }


    /*
    public function findOneBySomeField($value): ?FixedCosts
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
