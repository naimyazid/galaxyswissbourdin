<?php

namespace App\Repository;

use App\Entity\Expenses;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Expenses|null find($id, $lockMode = null, $lockVersion = null)
 * @method Expenses|null findOneBy(array $criteria, array $orderBy = null)
 * @method Expenses[]    findAll()
 * @method Expenses[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpensesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Expenses::class);
    }



    public function findExpansesNotPayed()
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.isPayed = :val')
            ->setParameter('val', false)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findExpansesByUser($user)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.visitorFirstName = :firstName')
            ->setParameter('firstName', $user->getFirstName())
            ->andWhere('e.visitorLastName = :lastName')
            ->setParameter('lastName', $user->getLastName())
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return Expenses[] Returns an array of Expenses objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Expenses
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
