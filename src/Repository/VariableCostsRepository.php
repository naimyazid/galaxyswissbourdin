<?php

namespace App\Repository;

use App\Entity\SearchCosts;
use App\Entity\VariableCosts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VariableCosts|null find($id, $lockMode = null, $lockVersion = null)
 * @method VariableCosts|null findOneBy(array $criteria, array $orderBy = null)
 * @method VariableCosts[]    findAll()
 * @method VariableCosts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VariableCostsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VariableCosts::class);
    }


    /**
     * @param SearchCosts $searchCosts
     * @return int|mixed|string
     * @throws \Exception
     */

    public function findByDate(SearchCosts $searchCosts)
    {
        $req = $this->createQueryBuilder('v');
        if($searchCosts->getMonth() && $searchCosts->getYear()){
            $req = $req->andWhere('v.user = :user')
                ->setParameter('user', $searchCosts->getUser())
                ->andWhere('v.createdAt >= :createdBegin')
                ->setParameter('createdBegin', new \DateTime($searchCosts->getYear().'-'. $searchCosts->getMonth().'-01' ))
                ->andWhere('v.createdAt <= :createdEnd')
                ->setParameter('createdEnd', new \DateTime($searchCosts->getYear().'-'. $searchCosts->getMonth().'-31' ));
        }
        return $req->getQuery()->getResult();

    }

    // /**
    //  * @return VariableCosts[] Returns an array of VariableCosts objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VariableCosts
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
