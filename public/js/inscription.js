$(() => {
  // // Validateur de formulaire javascript de Bootstrap
  formValidate(envoyer, invalidInput);

  // Afficher l'aide à la saisie quand si necessaire
  textHelp('#firstName', '#firstNameHelp');
  textHelp('#lastName', '#lastNameHelp');
  
  // afficher l'aide puour le mot de passe
  textHelp('#password', '#passwordHelp');

  //test
  function envoyer() {
    alert('votre formulaire a été envoyé avec succes!');
  }
  
  // $input = $('form input');
  // console.log($input);

});

