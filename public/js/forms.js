$(document).ready(function(){
    // On récupère le div contenur du formulaire des frais forfaitisés
    let $fixedCostsCollectionHolder = $('tbody.fixedCosts');
    let $variableCostsCollectionHolder = $('tbody.variableCosts');

    // On compte les entrées de formaulaire actuelle, et on l'utilise comme nouvel index lors de l'insertion d'un nouvel élément
    $fixedCostsCollectionHolder.data(
        'index',
        $fixedCostsCollectionHolder.find('input').length
    );
    $variableCostsCollectionHolder.data(
        'index',
        $variableCostsCollectionHolder.find('input').length
    );

    // Ajouter une ligne dans le formulaire des frais au forfait
    $('body').on('click', '.add_item_link', function(e){
        e.preventDefault();
        let $collectionHolderClass = $(e.currentTarget).data('collectionHolderClass');
        addFormToCollection($collectionHolderClass);
    });

    // Ajouter une ligne dans le formulaire des frais hors forfait
    $('body').on('click', '.add_item_link_one', function(e){
        e.preventDefault();
        let $collectionHolderClass = $(e.currentTarget).data('collectionHolderClass');
        addFormToCollection($collectionHolderClass);
    });

    // Supprimer une ligne
    $('body').on('click','.remove', function(e){
        e.preventDefault();
        $(e.currentTarget).parents('tr').slideUp(300, function(){
            $(this).remove();
        });
        // Mettre à jour le total des frais au forfait
        let $currentTotal = $(e.currentTarget).parent().prev().find('input.totalFixedCost').val();
        if (isNaN($currentTotal)) $currentTotal = 0;
        let $strGlobal = $("#global_fixedCosts").val();
        let $global = Number($strGlobal.substr(0, $strGlobal.length-1)) - $currentTotal;
        $("#global_fixedCosts").val($global + ' €');

        // Mettre à jour le total des frais hors forfait
        let $currentTotalOne = $(e.currentTarget).parent().prev().find('input.totalVariableCost').val();
        if (isNaN($currentTotalOne)) $currentTotalOne = 0;
        let $strGlobalOne = $('#global_variableCosts').val();
        let $globalOne = Number($strGlobalOne.substr(0, $strGlobalOne.length-1) - $currentTotalOne);
        $('#global_variableCosts').val($globalOne + ' €');
    });

    // Afficher le prix unitaire d'un frais au forfait
    $('body').on('change','select', addCost);

    // Calculer le montant total des frais au forfait
    $('body').on('change', function () {
        let $number = $fixedCostsCollectionHolder.find('tr').length;
        totalFixedCost($number);
    });

    // Calculer le total pour les frais au forfait enregistrés précédement
    let $number = $fixedCostsCollectionHolder.find('tr').length;
    for (let i=1; i<=$number; i++) {
        let $selectValue = $('#fixedCosts>tr:nth-child(' + i + ')').find('select').val();
        $('#fixedCosts>tr:nth-child(' + i + ')').children('td:nth-child(3)').find('input').val($selectValue);
    }
    totalFixedCost($number);

    // Calculer le montant des frais hors forfait
    $('body').on('change', function(){
       let $numberOne = $variableCostsCollectionHolder.find('tr').length;
       totalVariableCosts($numberOne);
    });
   // Calculer le total des frais hors forfait enregistrés précédement
    let $numberOne = $variableCostsCollectionHolder.find('tr').length;
   totalVariableCosts($numberOne);

})

//Ajouter un nouveau formulaire
function addFormToCollection($collectionHolderClass) {
    let $collectionHolder = $('.' + $collectionHolderClass);
    let prototype = $collectionHolder.data('prototype');
    let index = $collectionHolder.data('index');

    let newForm = prototype;
    newForm = newForm.replace(/__name__/g, index);
    $collectionHolder.data('index', index + 1);

    let $newFormTr = $('<tr></tr>').append(newForm);
    $collectionHolder.append($newFormTr)
}

// afficher le prix unitaire d'un frais au forfait
function addCost() {
    $(this).parents('td').next().next().find('input').val($(this).val());
}

// Calculer le total des frais au forfait
function totalFixedCost(number){
    // Montant global
    let $global = 0;
    //On indique le nombre de lignes a traiter pour le calcul
    for(let i=1 ; i<=number ; i++){
        let $quantity = $('#fixedCosts>tr:nth-child(' + i + ')').children('td:nth-child(2)').find('input').val();
        if (isNaN($quantity)) $quantity = 0;
        let $unitPrice = $('#fixedCosts>tr:nth-child(' + i + ')').children('td:nth-child(3)').find('input').val();
        if (isNaN($unitPrice)) $unitPrice = 0;
        let $total =Number($quantity) * Number($unitPrice);
        $('#fixedCosts>tr:nth-child(' + i + ')').children('td:nth-child(4)').find('input').val($total);
        $global = $global + $total;
    }
    $("#global_fixedCosts").val($global + " €");
}

// Calculer le total des frais hors forfait
function totalVariableCosts(number) {
        let $global = 0;
        for (let i = 1; i<= number; i++){
        let $total = $('#variableCosts>tr:nth-child(' + i + ')').children('td:nth-child(4)').find('input').val();
        if (isNaN($total)) $total = 0;
        $global = $global + Number($total);
    }
    $('#global_variableCosts').val($global + ' €');
}