$(document).ready(function(){
    // Injecter le formulaire dans la boite modal
    $('.data-trigger').on('click', function(e){
        e.preventDefault();
        $('#edite_cost').modal();
        let url = $(this).attr('data-target');
        $.post(
            url,
            function(data){
                $('#content').html(data);
                $('.modal1').modal('show');
                $('#js-form-edite-fixed-cost').attr('action', url);
            }
        )
    });

    // Soumission du formulaire en ajax
    $('#content').on('submit','#js-form-edite-fixed-cost', function(e){
        e.preventDefault();
        let $form = $('#js-form-edite-fixed-cost');
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $form.serialize() ,
            processData: false,
            ContentType: false,
            success: function(response){
                $('.modal1').modal('hide');
                let $unitPrice = $('#'+ response.id + '>.js-unitPrice').html();
                let $price = Number($unitPrice.substr(0, $unitPrice.length - 1));
                $('#'+response.id + '>.js-quantity').html(response.quantity);
                $('#'+response.id + '>.js-total').html(Number(response.quantity) * $price + ' €');
                $form.reset();
            },
            error: function(){
                alert('Quelque chose d\'imprévue vient de se passer, Veuillez reéssayer !');
            }
        });
    });
});
