var formValidate = (formSubmit, invalidInput) => {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    // var forms = document.getElementsByClassName('needs-validation');
    var forms = $('.needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
          if (invalidInput) invalidInput();
        } else {
          formSubmit();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
};

// Example starter JavaScript for disabling form submissions if there are invalid fields
// https://gist.github.com/wwwisie/faef68540a1e0decf37acd5cd5e98dfe
// https://gist.github.com/jmaicaaan/f559df25a8dedd8d846aa00d7c0ecfe7
// https://www.tutorialrepublic.com/twitter-bootstrap-tutorial/bootstrap-helper-classes.php