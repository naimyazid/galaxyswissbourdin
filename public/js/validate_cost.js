$(document).ready(function() {
    $('.js-validate-fixedCost').on('click', OnClickBtnValidate);
    $('.js-regret-fixedCost').on('click', OnClickBtnRegret);
    $('.js-validate-variableCost').on('click', OnClickBtnValidate);
    $('.js-regret-variableCost').on('click', OnClickBtnRegret);
    $('.js-report-variableCost').on('click', OnClickBtnReport);

   function OnClickBtnValidate(e){
        e.preventDefault();
        let $url = $(this).parent().attr('href');
        let $spanStatus = $(e.currentTarget).parents('td').prev().children('span.js-status');

        $.ajax({
            url: $url,
            type: 'POST',
            contentType: false,
            processData: false,
            error: function(err){
                alert('Quelquechose s\'est mal passé, veuillez reéssayer!');
            },
            success: function(response){
                $spanStatus.html(response.status);
                if($spanStatus.hasClass('badge-danger')){
                   $spanStatus.removeClass('badge-danger').addClass('badge-primary px-2');
                }
                if ($spanStatus.hasClass('badge-info')){
                    $spanStatus.removeClass('badge-info').addClass('badge-primary px-2');
                }
            }
        });
   }

    function OnClickBtnRegret(e){
        e.preventDefault();
        let $url = $(this).parent().attr('href');
        let $spanStatus = $(e.currentTarget).parents('td').prev().children('span.js-status');

        $.ajax({
            url: $url,
            type: 'POST',
            contentType: false,
            processData: false,
            error: function(err){
                alert('Quelquechose s\'est mal passé, veuillez reéssayer!');
            },
            success: function(response){
                $spanStatus.html(response.status);
                if($spanStatus.hasClass('badge-primary px-2')){
                    $spanStatus.removeClass('badge-primary px-2').addClass('badge-danger');
                }
                if ($spanStatus.hasClass('badge-info')){
                    $spanStatus.removeClass('badge-info').addClass('badge-danger');
                }
            }
        });
    }

    function OnClickBtnReport(e){
        e.preventDefault();
        let $url = $(this).parent().attr('href');
        let $spanStatus = $(e.currentTarget).parents('td').prev().children('span.js-status');

        $.ajax({
            url: $url,
            type: 'POST',
            contentType: false,
            processData: false,
            error: function(err){
                alert('Quelquechose s\'est mal passé, veuillez reéssayer!');
            },
            success: function(response){
                $spanStatus.html(response.status);
                if($spanStatus.hasClass('badge-primary px-2')){
                    $spanStatus.removeClass('badge-primary px-2').addClass('badge-info');
                }
                if ($spanStatus.hasClass('badge-danger')){
                    $spanStatus.removeClass('badge-danger').addClass('badge-info');
                }
            }
        });
    }





})