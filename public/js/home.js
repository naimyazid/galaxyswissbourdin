$(() =>{
  // Ce traitement nous permet de changer le menu hamburger en x et vice-versa
  $('.nav-button')
    .on('click', function (e) {
      e.preventDefault();
      $('.nav-button').toggleClass('change');
  });
})