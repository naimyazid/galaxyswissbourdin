$(document).ready(function() {
    $('.js-validate-expense').on('click', OnClickBtnValidate);

    function OnClickBtnValidate(e){
        e.preventDefault();
        let $url = $(this).attr('href');
        let $spanStatus = $(e.currentTarget).parents('tr').find('.js-validated-expense');

        $.ajax({
            url: $url,
            type: 'POST',
            contentType: false,
            processData: false,
            error: function(err){
                alert('Quelquechose s\'est mal passé, veuillez reéssayer!');
            },
            success: function(response){
                $spanStatus.html('Oui').removeClass('badge-danger').addClass('badge-primary');
            }
        });
    }

})